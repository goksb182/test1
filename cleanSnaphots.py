from datetime import datetime, timedelta, timezone
from dateutil.parser import parse
from botocore.exceptions import ClientError
import boto3
#from account_id import *
import os

# list of accounts
account_id = os.environ["ACCOUNT_ID"]
aws_accounts = account_id
aws_accounts = aws_accounts.split(",")

for account in aws_accounts:
    sts_client = boto3.client('sts')

    # Assuming the role
    assumed_role_object = sts_client.assume_role(
        RoleArn="arn:aws:iam::{}:role/dcs-AC3-ReadOnly-Role".format(
            account),
        RoleSessionName="Session"
    )
    credentials = assumed_role_object['Credentials']

    # setting credentials
    session = boto3.Session(
        aws_access_key_id=credentials['AccessKeyId'],
        aws_secret_access_key=credentials['SecretAccessKey'],
        aws_session_token=credentials['SessionToken'],
    )

    account_id = session.client('sts').get_caller_identity().get('Account')

    #ec2_client = session.client('ec2')
    ec2 = session.client('ec2', region_name='ap-southeast-2')

    response = ec2.describe_snapshots(OwnerIds=[account_id])
    snapshots = response['Snapshots']
    # print(snapshots)

    # sort snapshots by date ascending
    snapshots.sort(key=lambda x: x["StartTime"])

    for snapshot in snapshots:
        # print(snapid['SnapshotId'])
        id = snapshot['SnapshotId']
        description = snapshot['Description']
        # print(snapshot['Tags'])

        # Get snapshot age
        diff_time = (datetime.now(timezone.utc) - snapshot['StartTime']).days

        if diff_time > 90:
            if 'Tags' not in snapshot:
                try:
                    print("Deleting snapshot {}, {} days old\n Image Description: {}\n".format(
                        id, diff_time, description))
                    # CAUTION this deletes snap
                    # ec2.delete_snapshot(SnapshotId=id)
                except Exception as e:
                    if 'InvalidSnapshot.InUse' in e.message:
                        print("Snapshot {} in use, skipping.".format(id))
                        continue
